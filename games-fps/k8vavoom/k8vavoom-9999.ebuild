# Copyright 2022 Vasya Boytsov
# Distributed under the terms of the GNU General Public License v3

EAPI="7"
inherit cmake-utils git-r3 multilib desktop xdg toolchain-funcs
#CMAKE_ECLASS=cmake

#inherit cmake-multilib
DESCRIPTION="best engine to play doom fork of vavoom"
HOMEPAGE="https://www.doomworld.com/forum/topic/102766-k8vavoom-no-good-thing-ever-dies-2019-nov-26-build/"
EGIT_REPO_URI="https://repo.or.cz/k8vavoom.git"
LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64"
RDEPEND="
	media-libs/libmad
	media-libs/flac
	media-libs/libsdl2[X,opengl]
	media-libs/libvorbis
	media-libs/openal
	media-libs/opus
	media-libs/opusfile
"
DEPEND="
	dev-util/cmake
	${RDEPEND}
"
IUSE="system-alloc system-fluid master server vccrun"

CMAKE_BUILD_TYPE=RelWithDebInfo


src_prepare() {
	cmake-utils_src_prepare
}

src_configure() {
	local mycmakeargs=(
		-DBUILD_SHARED_LIBS=OFF
		-DWITH_MIMALLOC=!$(usex system-alloc)
		-DWITH_SYS_FLUID=$(usex system-fluid)
		-DCMAKE_INSTALL_PREFIX=/usr
		-DENABLE_MASTER=$(usex master)
		-DENABLE_SERVER=$(usex server)
		-DENABLE_VCCRUN=$(usex vccrun)
	)
	cmake-utils_src_configure
}

src_install() {
	make_desktop_entry "${PN}" "k8vavoom" "${PN}" "Game;ActionGame"
	cmake-utils_src_install
}
