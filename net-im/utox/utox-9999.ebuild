# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=7

inherit eutils git-r3 gnome2-utils toolchain-funcs cmake-utils

DESCRIPTION="Lightweight Tox client"
HOMEPAGE="http://utox.org"
EGIT_REPO_URI="https://github.com/uTox/uTox.git
	https://github.com/GrayHatter/uTox.git"

LICENSE="GPL-3"
SLOT="0"
IUSE="dbus filter_audio"

RDEPEND="net-libs/tox[av]
	media-libs/freetype
	filter_audio? ( media-libs/libfilteraudio )
	media-libs/libv4l
	media-libs/libvpx
	media-libs/openal
	x11-libs/libX11
	x11-libs/libXext
	dbus? ( sys-apps/dbus )"
DEPEND="${RDEPEND}
	virtual/pkgconfig"

src_prepare() {
	cmake-utils_src_prepare
}

src_configure() {
        local mycmakeargs=(
                -DENABLE_DBUS=$(usex dbus)
                -DENABLE_FILTERAUDIO=$(usex filter_audio)
                -DENABLE_TESTS=OFF
        )
cmake-utils_src_configure
	# respect CFLAGS
}




