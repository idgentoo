# Copyright 1999-2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

PYTHON_COMPAT=( python3_{6,7,8,9,10} )

inherit linux-info systemd git-r3 python-single-r1

DESCRIPTION="Fix Intel CPU Throttling on Linux"
HOMEPAGE="https://github.com/erpalma/throttled"
EGIT_REPO_URI="${HOMEPAGE}.git"
EGIT_BRANCH="master"
KEYWORDS="~amd64"
LICENSE="MIT"
SLOT="0"
IUSE=""
REQUIRED_USE="${PYTHON_REQUIRED_USE}"

RDEPEND="${PYTHON_DEPS}
	$(python_gen_cond_dep '
		dev-python/dbus-python[${PYTHON_USEDEP}]
		dev-python/pygobject[${PYTHON_USEDEP}]
	')
"
DEPEND="${PYTHON_DEPS}"
CONFIG_CHECK="X86_MSR DEVMEM"

pkg_setup() {
	linux-info_pkg_setup
	python-single-r1_pkg_setup
}

src_prepare() {
	default
	sed -i -e "s/ExecStart=.*/ExecStart=${PN}/" systemd/throttled.service
}

src_install() {
	default
	python_domodule mmio.py
	python_newscript throttled.py ${PN}
	dodoc README.md
	insinto /etc/
	doins etc/throttled.conf
	doinitd "${FILESDIR}"/"${PN}"
	systemd_newunit "${S}/systemd/throttled.service" "${PN}".service
}
